import numpy as np

# Collect element stiffness matrices

def stima3(vertices, lam): # Element stiffness matrix for simplex
  d = vertices.shape[1]
  L = np.array( [ [lam(0), lam(1)], [lam(1), lam(2)]] )
  C = np.block( [ [np.ones( (1, d+1) )], [ vertices.transpose() ] ] )
  G = np.linalg.solve(C, np.block( [ [np.zeros((1,d)) ], [np.identity( d )] ] ) )
  M = np.linalg.det(C) * np.matmul( np.matmul(G, L), np.transpose( G ) ) / np.math.factorial(d)
  return M

  # Modification needed: 
  # was: const * G * G' -> const * G * L * G' (L stores conductivities)