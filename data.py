import numpy as np

# Module to define all data of the problem

def u_D( x ): # Dirichlet boundary data 
    return np.zeros(( x.shape[0], 1))

def g( x ): # Neumann boundary data
    return 0

def f( x ): # Source term
    return 1

def u_D_hmg( x ): # Dirichlet boundary data 
    return np.zeros(( x.shape[0], 1))