# -*- coding: utf-8 -*-

# Imports
import numpy as np
from numpy.core.fromnumeric import transpose
from numpy.lib.arraysetops import unique
from scipy.sparse import lil_matrix, csr_matrix, bmat
from scipy.sparse import linalg 
from scipy.sparse.linalg import spsolve

# Local functions
from show import show
from data import u_D_hmg 
from stima import stima3_hmg, rhs3_hmg_mult_rhs, hmg3_mult_rhs

# Load all data
coordinates = np.loadtxt ( 'coordinates_hmg.dat', comments = '%' )
elements3 = np.loadtxt( 'elements3_hmg.dat', dtype = 'int', comments = '%' )
dirichlet = np.loadtxt( 'dirichlet_hmg.dat', dtype = 'int', comments = '%' )
# New data needed for homogenization
phases = np.loadtxt ( 'phases_hmg.dat', comments = '%' )
periodicity = np.loadtxt ( 'periodicity_hmg.dat', dtype = 'int', comments = '%' )

# delete first columns from coordinates, elements3, elements4, and neumann arrays
coordinates = np.delete( coordinates, 0, 1 )
elements3 = np.delete( elements3, 0, 1 )
dirichlet = np.delete( dirichlet, 0, 1 )
phases = np.delete( phases, 0, 1 )
periodicity = np.delete( periodicity, 0, 1 )

# setup matrices
FreeNodes = np.setdiff1d( range( coordinates.shape[0]), np.unique( dirichlet ) )
A = lil_matrix( np.zeros( (coordinates.shape[0], coordinates.shape[0]) ) ) 
b = np.zeros( (coordinates.shape[0], 2) )  
W = np.zeros( (coordinates.shape[0], 2) )

# assembly of stiffness matrices and volume forces (triangular elements)
for j in range( elements3.shape[0] ):
   A[ np.ix_( elements3[j, 0:3], elements3[j, 0:3] ) ] += stima3_hmg( coordinates[ elements3[ j, 0:3], ], phases[ elements3[j, 3], ] )
   b[ elements3[j, 0:3], ] += rhs3_hmg_mult_rhs( coordinates[ elements3[ j, 0:3], ], phases[ elements3[j, 3], ] )

# Prepare tying matrix T
T = np.zeros( (periodicity.shape[0], coordinates.shape[0]) )

for j in range( periodicity.shape[0] ):
  T[j, periodicity[j, :]] = [1, -1]

# Setup the partitioned system
A_hmg = bmat( [ [ A[ np.ix_( FreeNodes, FreeNodes) ], T[:, FreeNodes ].transpose() ], \
  [ T[:, FreeNodes ], None ] ] )
b_hmg = np.block( [ [ b[ FreeNodes, ] ], [ np.zeros( ( T.shape[0], 2 ) ) ] ] )

u_hmg = spsolve( A_hmg.tocsr(), b_hmg ) # Solve the partitioned system

W[ FreeNodes, ] = u_hmg[ 0:FreeNodes.shape[0] ]   # Extract only the solution, not Lagrange multipliers

# graphic representation
show(elements3[:, 0:3], np.array([]), coordinates, W[:, 0])
show(elements3[:, 0:3], np.array([]), coordinates, W[:, 1])

# Perform homogenization
L_eff = np.zeros( (2, 2) ) # Effective conductivity matrix
A = 0. # Area of RVE

for j in range( elements3.shape[0] ): 
  L_eff_e, A_e = hmg3_mult_rhs( coordinates[ elements3[ j, 0:3], ], phases[ elements3[j, 3], ], \
                          W[ elements3[ j, 0:3 ], ], ) 
  L_eff += L_eff_e
  A += A_e

L_eff /= A # Normalize with respect to volume

# Output the result
print( L_eff )
print( A )

# Analytical formula for homogenization
c = 0.3 # Volume fraction for the first phase
print( 1./ ( c / phases[0, 0] + (1 - c) / phases[1, 0] ) )
print( c * phases[0, 2] + (1 - c ) * phases[1, 2])