import numpy as np

def flux_pwrlaw(lam, p, g):
    q = - lam * (np.linalg.norm(g) ** (p - 1)) * g 
    return q

def tangent_pwrlaw(lam, p, g):
    D = np.zeros((2, 2))
    tmp = -lam * ( np.linalg.norm(g) ** (p-3))
    D[0, 0] = tmp*( p*g[0]**2 + g[1]**2)
    D[0, 1] = tmp*(p-1)*g[0]*g[1]
    D[1, 0] = D[0, 1]
    D[1, 1] = tmp*( p*g[1]**2 + g[0]**2)
    return D 