# Imports
import numpy as np
from numpy.lib.arraysetops import unique
from scipy.sparse import lil_matrix, csr_matrix, bmat
from scipy.sparse import linalg 
from scipy.sparse.linalg import spsolve

# Local functions
from show import show
from data import u_D_hmg 
from stima import f_int3_hmg_rhs_pwrlaw, stima3_hmg_rhs_pwrlaw

# Load all data
coordinates = np.loadtxt ( 'coordinates_hmg_code.dat', comments = '%' )
elements3 = np.loadtxt( 'elements3_hmg.dat', dtype = 'int', comments = '%' )
dirichlet = np.loadtxt( 'dirichlet_hmg.dat', dtype = 'int', comments = '%' )

# New data needed for homogenization
phases = np.loadtxt ( 'phases_hmg_pwrlaw.dat', comments = '%' )
periodicity = np.loadtxt ( 'periodicity_hmg.dat', dtype = 'int', comments = '%' )
ID = coordinates[:, 3]
ID = ID.astype(int)

# Solution parameters
tolerance = 1e-6
maxiter = 100 

# delete first columns from coordinates, elements3, elements4, and neumann arrays
coordinates = np.delete( coordinates, 3, 1 ) # last colum was stored in ID
coordinates = np.delete( coordinates, 0, 1 )
elements3 = np.delete( elements3, 0, 1 )
dirichlet = np.delete( dirichlet, 0, 1 )
phases = np.delete( phases, 0, 1 )
periodicity = np.delete( periodicity, 0, 1 )

# setup matrices
ndof = max( ID ) + 1

K = lil_matrix( np.zeros( (ndof, ndof) ) )
f_int = np.ones( (ndof, 1) )  
d = np.zeros( (ndof, 1) )
dd = np.zeros( (ndof, 1) )

# Average gradient
G = [[0], [1]]

# Newton loop 
for k in range( maxiter ):

  # Assemble RHS
  f_int[...] = 0
  for e in range( elements3.shape[0] ): 
    IDe = ID[elements3[e, 0:3]] # Element code numbers 
    isIDe = IDe > -1 # Active code numbers

    de = np.zeros( (3, 1))
    de[ isIDe ] = d[ IDe[isIDe] ]

    be = f_int3_hmg_rhs_pwrlaw( coordinates[ elements3[e, 0:3], ], phases[ elements3[e, 3], ], de, G ) # element RHS
    f_int[ IDe[ isIDe ] ] += be[ isIDe ]

  norm = np.linalg.norm(f_int)

  print( "> Iteration = ", str(k), " norm = ", str(norm) )

  if(norm < tolerance):
    break

  # Assemble tangent stiffness matrix
  K[...] = 0

  for e in range( elements3.shape[0] ): 
    IDe = ID[elements3[e, 0:3]] # Element code numbers 
    isIDe = IDe > -1 # Active code numbers

    de = np.zeros( (3, 1))
    de[ isIDe ] = d[ IDe[isIDe] ]

    Ke = stima3_hmg_rhs_pwrlaw( coordinates[ elements3[e, 0:3], ], phases[ elements3[e, 3], ], de, G ) # element RHS
    K[ np.ix_( IDe[ isIDe ], IDe[ isIDe ] ) ] += Ke[ np.ix_(isIDe, isIDe) ]

  # Solve for Newton step
  dd = spsolve( K.tocsr(), -f_int ) # Solve the partitioned system
  d += dd.reshape(ndof, 1)

# graphical representation
d_plt = np.zeros( (coordinates.shape[0], 1) ) # Reshape solution for plotting
d_plt[ ID > -1, ] = d[ ID[ ID > -1] ] # Copy data

show(elements3[:, 0:3], np.array([]), coordinates, d_plt)