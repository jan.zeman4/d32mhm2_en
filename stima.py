from operator import matmul
import numpy as np
import consitutive as cs

# Collect element stiffness matrices

def stima3(vertices): # Element stiffness matrix for simplex
  d = vertices.shape[1]
  C = np.block( [ [np.ones( (1, d+1) )], [ vertices.transpose() ] ] )
  G = np.linalg.solve(C, np.block( [ [np.zeros((1,d)) ], [np.identity( d )] ] ) )
  M = np.linalg.det(C) * np.matmul( G, np.transpose( G ) ) / np.math.factorial(d)
  return M

def stima4( vertices ):
  D_Phi = np.block( [ [ vertices[ 1, ]-vertices[ 0, ] ], [ vertices[ 3, ]-vertices[ 0, ] ] ])
  B = np.linalg.inv( np.matmul( np.transpose( D_Phi) , D_Phi ) )
  
  C1 = np.array( [[2,-2], [-2,2]] ) * B[0,0] + \
    np.array( [[3,0], [0,-3]] ) * B[0,1] + np.array( [[2,1], [1,2]] ) * B[1,1]
  C2 = np.array( [[-1,1], [1,-1]] ) *B[0,0] + \
    np.array( [[-3,0], [0,3]] ) * B[0,1] + np.array( [[-1,-2], [-2,-1]] ) * B[1,1]

  C = np.block( [ [C1, C2], [C2, C1] ] )

  M = np.linalg.det( D_Phi ) * C / 6
  return M

# Collect element stiffness matrices
def stima3_hmg(vertices, lam): # Element stiffness matrix for simplex
  d = vertices.shape[1]
  L = np.array( [ [lam[0], lam[1]], [lam[1], lam[2]] ] )
  C = np.block( [ [np.ones( (1, d+1) )], [ vertices.transpose() ] ] )
  G = np.linalg.solve(C, np.block( [ [np.zeros((1,d)) ], [np.identity( d )] ] ) )
  
  M = np.linalg.det(C) * np.matmul( np.matmul( G, L ), np.transpose( G ) ) / np.math.factorial(d)
  return M

def rhs3_hmg( vertices, lam, E ): # RHS matrix for simplex
  d = vertices.shape[1]
  L = np.array( [ [lam[0], lam[1]], [lam[1], lam[2]] ] )
  C = np.block( [ [np.ones( (1, d+1) )], [ vertices.transpose() ] ] )
  G = np.linalg.solve(C, np.block( [ [np.zeros((1,d)) ], [np.identity( d )] ] ) )
  
  b = -np.linalg.det(C) * np.matmul( np.matmul( G, L ), E ) / np.math.factorial(d)

  # Note that this substantially overlaps with stima3_hmg
  return b

def rhs3_hmg_mult_rhs( vertices, lam ): # RHS matrix for simplex
  d = vertices.shape[1]
  L = np.array( [ [lam[0], lam[1]], [lam[1], lam[2]] ] )
  C = np.block( [ [np.ones( (1, d+1) )], [ vertices.transpose() ] ] )
  G = np.linalg.solve(C, np.block( [ [np.zeros((1,d)) ], [np.identity( d )] ] ) )
  
  b = -np.linalg.det(C) / np.math.factorial(d) * np.matmul( G, L ) 

  # Note that this substantially overlaps with stima3_hmg
  return b  

def hmg3_mult_rhs( vertices, lam, W ): # Contribution of element to effective matrix
  d = vertices.shape[1]

  L = np.array( [ [lam[0], lam[1]], [lam[1], lam[2]] ] )
  C = np.block( [ [np.ones( (1, d+1) )], [ vertices.transpose() ] ] )
  G = np.linalg.solve(C, np.block( [ [np.zeros((1,d)) ], [np.identity( d )] ] ) )
  I = np.eye( d )

  A = np.linalg.det(C) / np.math.factorial(d)
  B = I + np.matmul( np.transpose(G), W )

  Leff = A * np.matmul( B, np.matmul(L, np.transpose(B) ) )

  return Leff, A

def flux3( vertices, lam, u ): # u is FULL solution (not fluctuation)
  d = vertices.shape[1]

  L = np.array( [ [lam[0], lam[1]], [lam[1], lam[2]] ] )
  C = np.block( [ [np.ones( (1, d+1) )], [ vertices.transpose() ] ] )
  G = np.linalg.solve(C, np.block( [ [np.zeros((1,d)) ], [np.identity( d )] ] ) )
  A = np.linalg.det(C) / np.math.factorial(d) # Element area
  
  Q = np.matmul( L, np.matmul( np.transpose(G), u ) ) # Q = L * grad u
  
  return Q, A

def f_int3_hmg_rhs_pwrlaw(vertices, phase_params, de, G):
  d = vertices.shape[1]

  C = np.block( [ [np.ones( (1, d+1) )], [ vertices.transpose() ] ] ) # Auxiliary matrix
  B = np.linalg.solve(C, np.block( [ [np.zeros((1,d)) ], [np.identity( d )] ] ) )
  A = np.linalg.det(C) / np.math.factorial(d) # Element area

  lam = phase_params[0]
  p = phase_params[1]
  g = G + np.matmul( np.transpose(B), de )

  f_int = A * np.matmul( B, cs.flux_pwrlaw(lam, p, g) )
    
  return f_int

def stima3_hmg_rhs_pwrlaw(vertices, phase_params, de, G):
  d = vertices.shape[1]

  C = np.block( [ [np.ones( (1, d+1) )], [ vertices.transpose() ] ] ) # Auxiliary matrix
  B = np.linalg.solve(C, np.block( [ [np.zeros((1,d)) ], [np.identity( d )] ] ) )
  A = np.linalg.det(C) / np.math.factorial(d) # Element area

  lam = phase_params[0]
  p = phase_params[1]
  g = G + np.matmul(np.transpose(B), de)

  K = np.matmul( cs.tangent_pwrlaw(lam, p,g), np.transpose(B) )
  K = A * np.matmul(B, K)

  return K