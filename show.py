# https://matplotlib.org/stable/gallery/mplot3d/trisurf3d_2.html

import matplotlib.pyplot as plt
import matplotlib.tri as mtri
from mpl_toolkits import mplot3d
import numpy as np

def show(elements3,elements4, coordinates, u):

    all_elems = None

    if elements3.size != 0:
        all_elems = elements3
    
    if elements4.size != 0:
        all_elems = np.block( [ [ all_elems ] , [ elements4[:, [0, 1, 2]] ], [ elements4[:, [2, 3, 0]] ] ] ) 

    triang = mtri.Triangulation(coordinates[:, 0], coordinates[:, 1], all_elems)

    fig = plt.figure()
    ax = fig.gca(projection='3d')

    ax.plot_trisurf(triang, u.flatten(), 
                linewidth = 0.2, 
                antialiased = True )

    ax.view_init( 40, 10 )
    ax.set_title( 'Solution of the Problem' )

    plt.show()

    return