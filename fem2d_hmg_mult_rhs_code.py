# Imports
import numpy as np
from numpy.lib.arraysetops import unique
from scipy.sparse import lil_matrix, csr_matrix, bmat
from scipy.sparse import linalg 
from scipy.sparse.linalg import spsolve

# Local functions
from show import show
from data import u_D_hmg 
from stima import stima3_hmg, rhs3_hmg_mult_rhs, hmg3_mult_rhs

# Load all data
coordinates = np.loadtxt ( 'coordinates_hmg_code.dat', comments = '%' )
elements3 = np.loadtxt( 'elements3_hmg.dat', dtype = 'int', comments = '%' )
dirichlet = np.loadtxt( 'dirichlet_hmg.dat', dtype = 'int', comments = '%' )

# New data needed for homogenization
phases = np.loadtxt ( 'phases_hmg.dat', comments = '%' )
periodicity = np.loadtxt ( 'periodicity_hmg.dat', dtype = 'int', comments = '%' )
ID = coordinates[:, 3]
ID = ID.astype(int)

# delete first columns from coordinates, elements3, elements4, and neumann arrays
coordinates = np.delete( coordinates, 3, 1 ) # last colum was stored in ID
coordinates = np.delete( coordinates, 0, 1 )
elements3 = np.delete( elements3, 0, 1 )
dirichlet = np.delete( dirichlet, 0, 1 )
phases = np.delete( phases, 0, 1 )
periodicity = np.delete( periodicity, 0, 1 )

# setup matrices
ndof = max( ID ) + 1

K = lil_matrix( np.zeros( (ndof, ndof) ) )
b = np.zeros( (ndof, 2) )  
W = np.zeros( (ndof, 2) )

# --- I stopped here ---

# assembly of stiffness matrices and volume forces (triangular elements)
# https://stackoverflow.com/questions/13029029/getting-only-those-values-that-fulfill-a-condition-in-a-numpy-array

for e in range( elements3.shape[0] ):
  Ke = stima3_hmg( coordinates[ elements3[e, 0:3], ], phases[ elements3[e, 3], ] ) # element conductivity matrix 
  be = rhs3_hmg_mult_rhs( coordinates[ elements3[e, 0:3], ], phases[ elements3[e, 3], ] ) # element RHS

  IDe = ID[elements3[e, 0:3]] # Element code numbers 
  isIDe = IDe > -1 # Active code numbers

  # Perform assembly
  K[ np.ix_( IDe[ isIDe ], IDe[ isIDe ] ) ] += Ke[ np.ix_(isIDe, isIDe) ]
  b[ IDe[ isIDe ], ] += be[ isIDe, ]

W = spsolve( K.tocsr(), b ) # Solve the partitioned system

print( W )

# Perform homogenization
L_eff = np.zeros( (2, 2) ) # Effective conductivity matrix
Y = 0. # RVE area

We = np.zeros( (3, 2) ) # Components of fluctationg fields

for e in range( elements3.shape[0] ):
  IDe = ID[elements3[e, 0:3]] # Element code numbers 
  isIDe = IDe > -1 # Active code numbers

  We[ :, ] = 0
  We[ isIDe, ] = W[ IDe[ isIDe ], ] # Extract corresponding components

  L_effe, Ae = hmg3_mult_rhs( coordinates[ elements3[ e, 0:3], ], phases[ elements3[e, 3], ], \
                          We )
  Y += Ae
  L_eff += L_effe

L_eff /= Y # Normalize with respect to RVE area

print(L_eff)

# Check homogenized conductivity matrix [from Part I]
c = 0.3 # From input

L_eff_a = np.zeros( (2, 2) ) # Effective conductivity matrix

L_eff_a[0, 0] = 1./( c / phases[0, 0] + (1 - c) / phases[1, 0] ) # Reuss
L_eff_a[1, 1] = c*phases[0, 2] + (1-c)*phases[1, 2] # Voigt

print( L_eff_a )
print( np.linalg.norm( L_eff - L_eff_a ) ) # To be checked

# graphical representation
W_plt = np.zeros( (coordinates.shape[0], 2) ) # Reshape solution for plotting
W_plt[ ID > -1, ] = W[ ID[ ID > -1], ] # Copy data

show(elements3[:, 0:3], np.array([]), coordinates, W_plt[:, 0])
show(elements3[:, 0:3], np.array([]), coordinates, W_plt[:, 1])